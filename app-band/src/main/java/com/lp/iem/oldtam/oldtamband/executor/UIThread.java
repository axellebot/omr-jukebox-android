package com.lp.iem.oldtam.oldtamband.executor;

import com.lp.iem.oldtam.domain.executor.PostExecutionThread;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by romai on 01/06/2017.
 */

@Singleton
public class UIThread implements PostExecutionThread {
    @Inject
    public UIThread() {}

    @Override public Scheduler getScheduler() {
        return AndroidSchedulers.mainThread();
    }
}
