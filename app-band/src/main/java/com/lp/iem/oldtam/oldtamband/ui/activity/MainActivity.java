package com.lp.iem.oldtam.oldtamband.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.lp.iem.oldtam.oldtamband.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
