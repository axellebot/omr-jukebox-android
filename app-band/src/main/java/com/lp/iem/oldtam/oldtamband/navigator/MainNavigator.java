package com.lp.iem.oldtam.oldtamband.navigator;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Build;
import android.transition.Fade;

import com.lp.iem.oldtam.oldtamband.R;

/**
 * Created by romai on 01/06/2017.
 */

public class MainNavigator {
    //public final static int FRAGMENT_LIST = 0;

    private FragmentManager fragmentManager;
    private Activity activity;

    private int currentFragmentId;

    public MainNavigator(Activity activity) {
        this.activity = activity;
        this.fragmentManager = this.activity.getFragmentManager();
    }

    public void start() {

    }

    public void resume() {

    }

    public void pause() {

    }

    public void stop() {

    }

    public void destroy() {

    }

    public void onBackPressed(){

    }

    public Fragment getCurrentFragment() {
        return fragmentManager.findFragmentById(R.id.fragmentContainer);
    }

    private void fragmentTransactionReplace(Fragment fragment) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, fragment, fragment.getClass().getName());
        fragmentTransaction.commit();
    }

    private void fragmentTransactionAdd(Fragment fragment) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fragment.setEnterTransition(new Fade());
            getCurrentFragment().setExitTransition(new Fade());
        }
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragmentContainer, fragment, fragment.getClass().getName());
        fragmentTransaction.addToBackStack("details");
        fragmentTransaction.commit();
    }
}
