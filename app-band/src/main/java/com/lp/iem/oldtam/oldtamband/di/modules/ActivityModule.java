package com.lp.iem.oldtam.oldtamband.di.modules;

import android.app.Activity;

import com.lp.iem.oldtam.oldtamband.di.PerActivity;
import com.lp.iem.oldtam.oldtamband.ui.activity.MainActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by romai on 01/06/2017.
 */

@Module
public class ActivityModule {

    private final MainActivity activity;

    public ActivityModule(MainActivity activity) {
        this.activity = activity;
    }

    /**
     * Expose the activity to dependents in the graph.
     */
    @Provides
    @PerActivity
    Activity activity() {
        return this.activity;
    }
}
