package com.lp.iem.oldtam.oldtampublic.ui.fragment;

import android.app.Fragment;

import com.lp.iem.oldtam.oldtampublic.ui.view.SongListView;

/**
 * Created by romai on 01/06/2017.
 */

public class SongListFragment extends Fragment implements SongListView {
    public static SongListFragment newInstance() {
        return new SongListFragment();
    }
}
