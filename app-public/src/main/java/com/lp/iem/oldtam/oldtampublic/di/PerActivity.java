package com.lp.iem.oldtam.oldtampublic.di;

import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by romai on 01/06/2017.
 */

@Scope
@Retention(RUNTIME)
public @interface PerActivity {}
