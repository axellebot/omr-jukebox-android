package com.lp.iem.oldtam.oldtampublic;

import android.app.Application;

import com.lp.iem.oldtam.oldtampublic.di.components.ApplicationComponent;
import com.lp.iem.oldtam.oldtampublic.di.components.DaggerApplicationComponent;
import com.lp.iem.oldtam.oldtampublic.di.modules.ApplicationModule;

/**
 * Created by romai on 01/06/2017.
 */

public class JBApplication extends Application {
    private static JBApplication application;
    public static JBApplication app() {
        return application;
    }
    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
        this.initializeInjector();
    }

    private void initializeInjector() {
        this.applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
