package com.lp.iem.oldtam.oldtampublic.di.modules;

import android.content.Context;

import com.lp.iem.oldtam.data.executor.JobExecutor;
import com.lp.iem.oldtam.data.repository.DataRepositoryImpl;
import com.lp.iem.oldtam.domain.executor.PostExecutionThread;
import com.lp.iem.oldtam.domain.executor.ThreadExecutor;
import com.lp.iem.oldtam.domain.repository.DataRepository;
import com.lp.iem.oldtam.oldtampublic.JBApplication;
import com.lp.iem.oldtam.oldtampublic.executor.UIThread;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by romai on 01/06/2017.
 */

@Module
public class ApplicationModule {
    private final JBApplication application;

    public ApplicationModule(JBApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return this.application;
    }

    @Provides @Singleton
    ThreadExecutor provideThreadExecutor(JobExecutor jobExecutor) {
        return jobExecutor;
    }

    @Provides @Singleton
    PostExecutionThread providePostExecutionThread(UIThread uiThread) {
        return uiThread;
    }

    @Provides @Singleton
    DataRepository provideDataRepository(DataRepositoryImpl dataRepository) {
        return dataRepository;
    }
}
