package com.lp.iem.oldtam.oldtampublic.di.components;

import android.content.Context;

import com.lp.iem.oldtam.domain.executor.PostExecutionThread;
import com.lp.iem.oldtam.domain.executor.ThreadExecutor;
import com.lp.iem.oldtam.domain.repository.DataRepository;
import com.lp.iem.oldtam.oldtampublic.di.modules.ApplicationModule;
import com.lp.iem.oldtam.oldtampublic.ui.activity.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by romai on 01/06/2017.
 */

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    void inject(MainActivity activity);

    //exposed to subgraph
    Context context();
    ThreadExecutor threadExecutor();
    PostExecutionThread postExecutionThread();
    DataRepository dataRepository();
}