package com.lp.iem.oldtam.oldtampublic.di.components;

import android.app.Activity;

import com.lp.iem.oldtam.oldtampublic.di.PerActivity;
import com.lp.iem.oldtam.oldtampublic.di.modules.ActivityModule;
import com.lp.iem.oldtam.oldtampublic.ui.fragment.SongListFragment;

import dagger.Component;

/**
 * Created by romai on 01/06/2017.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {
    Activity activity();

    void inject(SongListFragment songListFragment);
}