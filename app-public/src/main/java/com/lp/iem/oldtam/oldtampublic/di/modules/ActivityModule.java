package com.lp.iem.oldtam.oldtampublic.di.modules;

import android.app.Activity;

import com.lp.iem.oldtam.oldtampublic.di.PerActivity;
import com.lp.iem.oldtam.oldtampublic.ui.activity.MainActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by romai on 01/06/2017.
 */

@Module
public class ActivityModule {

    private final MainActivity activity;

    public ActivityModule(MainActivity activity) {
        this.activity = activity;
    }

    /**
     * Expose the activity to dependents in the graph.
     */
    @Provides
    @PerActivity
    Activity activity() {
        return this.activity;
    }
}
