package com.lp.iem.oldtam.domain.executor;

import java.util.concurrent.Executor;

/**
 * Created by amiltonedev_dt013 on 21/03/2017.
 */

public interface ThreadExecutor extends Executor {
}
