package com.lp.iem.oldtam.domain.executor;

import rx.Scheduler;

/**
 * Created by amiltonedev_dt013 on 21/03/2017.
 */

public interface PostExecutionThread {
    Scheduler getScheduler();
}
