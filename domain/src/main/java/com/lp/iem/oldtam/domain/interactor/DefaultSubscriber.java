package com.lp.iem.oldtam.domain.interactor;

/**
 * Created by Milihhard on 26/04/2017.
 */

public class DefaultSubscriber<T> extends rx.Subscriber<T> {
    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {

    }

    @Override
    public void onNext(T t) {

    }
}
